
function onMessage(evt){
	try{

	}catch(Ex){
		console.log(Ex);
	}
}

var gender_selection, age, length, weight, hair_color, eye_color, education, smoking, alcohol, sport, CsvData, count, lang;

jQuery(document).ready(function(){
	lang = 'et';
	// -- Load data
	loadResults();

	// -- Get EST translations
	getTranslations('et');

	// -- Languages click function
	jQuery('.languages div').click(function(){
		lang = jQuery(this).attr('data-language');
		console.log(lang);
		if(lang){
			getTranslations(lang);
		}
	});

	// -- Row button click
	jQuery('.row_nupud').click(function(){
		setValue(jQuery(this).attr('data-appearance'), jQuery(this).attr('data-value'));
		controllValues();
	});

});

function controllValues(){
	//console.log(length, weight, hair_color, eye_color);
	if(length != null && weight != null && hair_color != null && eye_color != null){
		//console.log(length, weight, hair_color, eye_color);
		$('.view[data-view="4"] button[data-button="transparent"]').addClass('visible');
	}
	if(education != null){
		$('.view[data-view="5"] button[data-button="transparent"]').addClass('visible');
	}
	if(smoking != null && alcohol != null && sport != null){
		$('.view[data-view="6"] button[data-button="transparent"]').addClass('visible');
	}

}

function setValue(appearance, value){
	$('.row_nupud[data-appearance="' + appearance + '"]').removeClass('selected_appearance');
	$('.row_nupud[data-appearance="' + appearance + '"][data-value="' + value + '"]').addClass('selected_appearance');

	if(appearance == 'length'){
		length = value;
	}
	if(appearance == 'weight'){
		weight = value;
	}
	if(appearance == 'hair_color'){
		hair_color = value;
	}
	if(appearance == 'eye_color'){
		eye_color = value;
	}
	if(appearance == 'education'){
		education = value;
	}
	if(appearance == 'smoking'){
		smoking = value;
	}
	if(appearance == 'alcohol'){
		alcohol = value;
	}
	if(appearance == 'sport'){
		sport = value;
	}
}

function loadResults(){
	$.ajax({
		type: "GET",
		url: "data/k12_new.csv",
		dataType: "text",
		success: function(res) {CsvData = res}
	});
}

function processData(allText) {
	//console.log('algus');
	count = null;
	var allTextLines = allText.split(/\r\n|\n/);
	var headers = allTextLines[0].split(',');
	for (var i=1; i<allTextLines.length; i++) {
		var data = allTextLines[i].split(',');
		//console.log(data[0]);
		if(	data[0] == gender_selection
				&& data[1] == age
				&& data[2] == length
				&& data[3] == weight
				&& data[4] == hair_color
				&& data[5] == eye_color
				&& data[6] == education
				&& data[7] == smoking
				&& data[8] == alcohol
				&& data[9] == sport

		){
			count = data[10];
			console.log(count);
		}
	}

}

function selectGender(gender){
	$('.choose_gender_images img').addClass('not_active');
	$('.choose_gender_images img[data-gender="' + gender + '"]').removeClass('not_active');
	gender_selection = gender;
	$('.view[data-view="2"] button[data-questionbtn="1"]').addClass('visible');
}

function goToView(viewNumber){
	$('.dragdealer').addClass('hidden');
	$(".view").removeClass("show");
	$(".view:nth-of-type("+viewNumber+")").addClass("show");
	if(viewNumber == 1){
		resetAllSamples();
	}
	if(viewNumber == 3){
		$('.dragdealer').removeClass('hidden');
		$('.translate_how_old_handler').attr('data-translation', 'kummast_soost_partner_' + gender_selection);
		getTranslations(lang);
	}
	if(viewNumber == 4){
		$('.translate_how_does_look_like').attr('data-translation', 'milline_valimus_' + gender_selection);
		getTranslations(lang);
	}
	if(viewNumber == 5){
		$('.translate_what_education').attr('data-translation', 'milline_voiks_olla_' + gender_selection);
		getTranslations(lang);
	}
	if(viewNumber == 6){

		$('.translate_what_lifestyle').attr('data-translation', 'milline_on_tema_eluviis_' + gender_selection);
		$('.translate_does_smoke').attr('data-translation', 'kas_ta_suitsetab_' + gender_selection);
		$('.translate_does_alcohol').attr('data-translation', 'kas_ta_tarbib_alk_' + gender_selection);
		$('.translate_does_phy_act').attr('data-translation', 'kas_ta_on_fyysiline_' + gender_selection);

		getTranslations(lang);
	}
	if(viewNumber == 7){
		processData(CsvData);
		setTimeout(function(){
			goToView(8);
		}, 5000);
	}
	if(viewNumber == 8){
		if(count != null){
			jQuery('.partners_count').html('<h2>' + count + '</h2>');
			jQuery('body > div:nth-child(8) > button').attr("onclick","goToView(9)");
			jQuery('body > div:nth-child(8) > button').attr("data-translation", "kuva_kontaktid");
			jQuery("body > div.view.show > p").attr("data-translation", "kas_soovid_yhendada");
			getTranslations(lang);

		} else {
			jQuery('.partners_count').html('<h2><5</h2>');
			jQuery('body > div:nth-child(8) > button').attr("onclick","goToView(1)");
			jQuery('body > div:nth-child(8) > button').attr("data-translation", "alusta_uuesti");
			jQuery("body > div.view.show > p").attr("data-translation", "less_than_five");
			getTranslations(lang);
			console.log(count);
		}

	}
}

// -- TRANSLATIONS
function getTranslations(lang) {
	jQuery('.languages div').removeClass('active');
	jQuery('.languages div[data-language="' + lang + '"]').addClass('active');
	var json = getLangJSON(lang);
	jQuery.each(json[0], function (key, data) {
		jQuery('[data-translation="' + key + '"]').html(data).val(data);
	});
}

function getLangJSON(lang){
	var json;
	if(lang == 'et'){
		json = translations.et;
	} else if(lang == 'en') {
		json = translations.en;
	} else if(lang == 'ru') {
		json = translations.ru;
	} else if(lang == 'fi') {
		json = translations.fi;
	}
	return json;
}

Number.prototype.map = function (in_min, in_max, out_min, out_max) {
	return (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

function getAge(x){
	var number = parseInt(round(x, 1)).map(0,100, 20,90);
	var min = Math.floor(number/5)*5;
	var max = min + 5;
	age = min + '-' + max;
	$('.min_age').html(min);
	$('.max_age').html(max);
}

function round(value, precision) {
	var multiplier = Math.pow(10, precision || 0);
	return Math.round(value * multiplier) / multiplier;
}

// Custom Checking Function..
function inRangeInclusive(start, end, value) {
	if (value <= end && value >= start)
		return value; // return given value
	return undefined;
}