var wsImpl = window.WebSocket || window.MozWebSocket;
var websocket1;
var websocket2;
var wsAttempts = 10;
var wsAttempt = 0;

var resetTimeout = null;
var resetTimeoutSeconds = 60*1000;

var customDragDealer;

$(function() {
   customDragDealer = new Dragdealer('simple-slider', {
      animationCallback: function (x, y) {
         getAge(x * 100);
      }
   });
})

$(function() {
	$(window).bind("click touchstart", function(){
		clearTimeout(resetTimeout);
		resetTimeout = setTimeout(function(){
			homeView();
		}, resetTimeoutSeconds);
	});
	document.onkeydown = checkKey;

});

function checkKey(e) {

}
function start(){
	if(websocket1 == undefined){
		setInterval(function(){checkWS(websocket1)}, 5000);
	}
	try{
		websocket1 = new wsImpl('ws://127.0.0.1:8181/');
		websocket1.onmessage = onMessage;
		websocket1.onopen = onOpen;
		websocket1.onclose = onClose;
		websocket1.onerror = onClose;
	}
	catch(e){
		console.log(e);
	}

	if(websocket2 == undefined){
		setInterval(function(){checkWS(websocket2)}, 5000);
	}
	try{
		websocket2 = new wsImpl('ws://127.0.0.1:8182/');
		websocket2.onmessage = onMessage;
		websocket2.onopen = onOpen;
		websocket2.onclose = onClose;
		websocket2.onerror = onClose;
	}
	catch(e){
		console.log(e);
	}

}

function checkWS(websocket){
	if(websocket.readyState != websocket.OPEN){
		if(wsAttempt<wsAttempts){
			websocket.close();
			start();
		}
		else{
			window.location.reload();
		}
		wsAttempt++;
	}
}
function onOpen(evt){
	/*_d(evt);*/
}
function onClose(evt){
	evt.target.close();
}
function onMessage(evt){
	try{
		console.log(evt);
	}catch(Ex){
		console.log(Ex);
	}
}
function sendAction(act){
	send(JSON.stringify({action: act}));
}

function send(data){
	try{
		websocket1.send(data);
		websocket2.send(data);
	}
	catch(e){}
}
function nextView(view){
	if(view.next().filter('.view').length != 0){
		$(".view").removeClass("show");
		view.next().filter('.view').addClass("show");
	}
}
function prevView(view){
	if(view.prev().filter('.view').length != 0){
		$(".view").removeClass("show");
		view.prev().filter('.view').addClass("show");
	}
}
function homeView(){
	$(".view").removeClass("show");
	$(".view:first-of-type").addClass("show");
	$("#home").css("visibility", "hidden");
	resetAllSamples();
}

function resetAllSamples(){
	length = null; weight = null; hair_color = null; eye_color = null; education = null; smoking = null; alcohol = null; sport = null;
	$('.dragdealer').addClass('hidden');
	$('button[data-button="transparent"]').removeClass('visible');
	$('.choose_gender_images img').removeClass('not_active');
	$('.view[data-view="3"] button[data-button="transparent"]').addClass('visible');
	customDragDealer.setValue(0);
	$('.row_nupud').removeClass('selected_appearance');

}