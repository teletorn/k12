Tekstid lähevad alles tõlkimisse, palun alusta eesti keelega.

Tegemist on puu-tüüpi mänguga. St, alguses küsitakse vastajalt, et millisest soost inimest ta otsib. Seejärel vanusegruppi, seejärel otsitava pikkust, kaalu, tema naturaalset juuksevärvi, silmavärvi, suitsetamisstaatust (jah=1/ei=0), alkoholitarbimist(jah,ei) ning viimaks füüsilist aktiivsust. Seejärel väljastatakse viimases veerus nähtav hinnang, et geenivaramu hinnangul on eestis 2018.a seisuga selliseid inimesi nii palju.

Soo valimine käib ikoonil klikkides.
Vanuse valimine käib liuguriga. Loodan väga, et te saate vanuse valimise lehele sellise liuguri teha. Inimene liigutab näpuga lillat mummu ja alla kuvatakse vanusevahemik. Vahemikud saad kätte kirjaga kaasasolevast Word-dokumendist.
Valikute nupud võiksid selles mängus oma värvi lillaks muuta kuidagi animeeritult (näiteks nii: https://codemyui.com/animated-ghost-button-fill-effect/).

